# FocusWriter Themes #

This is a small collection of themes I use in [FocusWriter](https://gottcode.org/focuswriter/) 

---- 

## DOS Perfect ##

Gives the feel of an old DOS machine.

Uses the [Perfect DOS 437 font](https://www.dafont.com/perfect-dos-vga-437.font) from [Zeh Fernando](http://zehfernando.com/) 

----

## iaWriteresque ##

Gives the feel of iaWriter. (since there is no linux version)

This theme uses their freely available [Duospace font](https://github.com/iaolo/iA-Fonts/tree/master/iA%20Writer%20Duospace) 

----

## To Install: ##

Download a theme file (.fwtz file)

From the **FocusWriter** menu, choose **Settings** > **Themes**

Click on the **Custom** tab. This shows all of the user installed themes, if you haven't installed any, it will be empty.

Click the **Import** button, navigate to the theme and click **Open**

The theme is now installed.
